package lk.sylanka.shylankatoursapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Calendar;

public class HomeActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{

    Button btMap,btMe,btCm,btTrip;
    private  long backPressedTime;
    private Toast backTost;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Button button = (Button) findViewById(R.id.btnCal);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment datePicker = new lk.sylanka.shylankatoursapp.DatePicker();
                datePicker.show(getSupportFragmentManager(), "date picker");
            }
        });

        btMap=findViewById(R.id.btnMap);
        btMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(HomeActivity.this,MyLocationView.class);
                startActivity(intent);
            }
        });

        btMe=findViewById(R.id.btnMemo);
        btMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(HomeActivity.this,Memo.class);
                startActivity(intent);
            }
        });

        btCm=findViewById(R.id.btnCam);
        btCm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(HomeActivity.this,Cam.class);
                startActivity(intent);
            }
        });

        btTrip=findViewById(R.id.btnTrip);
        btTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(HomeActivity.this,Trip.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.btnTs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clicked_btn("http://www.traintime.lk");
            }
        });

        findViewById(R.id.btnW).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clicked_btn("https://www.msn.com/en-us/weather");
            }
        });
    }

    public void clicked_btn(String url)
    {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        startActivity(intent);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        java.util.Calendar c = java.util.Calendar.getInstance();
        c.set(java.util.Calendar.YEAR, year);
        c.set(java.util.Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        String currentDateString = DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime());
    }

    public void openActivityNav(View view){
        Intent intent=new Intent(this,NavDrw.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);
    }

    @Override
    public void onBackPressed() {
        if(backPressedTime+2000>System.currentTimeMillis()){
            backTost.cancel();
            ActivityCompat.finishAffinity(this);
        }
        else {
            backTost=Toast.makeText(getApplicationContext(),"Press back again to exit",Toast.LENGTH_SHORT);
            backTost.show();
        }
        backPressedTime=System.currentTimeMillis();
    }
}
