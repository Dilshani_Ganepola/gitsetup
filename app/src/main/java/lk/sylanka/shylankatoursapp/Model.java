package lk.sylanka.shylankatoursapp;

public class Model {

    private int image;
    private String title;
    private String dec;

    public Model(int image, String title, String dec) {
        this.image = image;
        this.title = title;
        this.dec = dec;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDec() {
        return dec;
    }

    public void setDec(String dec) {
        this.dec = dec;
    }
}
