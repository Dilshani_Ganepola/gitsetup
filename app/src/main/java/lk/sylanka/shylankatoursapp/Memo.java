package lk.sylanka.shylankatoursapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

public class Memo extends AppCompatActivity {

    EditText eT;
    Button btnA,btnR;
    RecyclerView Rv;

    List<MainData> dataList=new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    RoomDB database;
    MainAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memo);

        eT=findViewById(R.id.txtEdit);
        btnA=findViewById(R.id.btnAdd);
        btnR=findViewById(R.id.btnReset);
        Rv=findViewById(R.id.Recycler_view);

        database=RoomDB.getInstance(this);
        dataList=database.mainDao().getAll();

        linearLayoutManager=new LinearLayoutManager(this);
        Rv.setLayoutManager(linearLayoutManager);
        adapter=new MainAdapter(Memo.this,dataList);
        Rv.setAdapter(adapter);

        btnA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sText=eT.getText().toString().trim();
                if (!sText.equals("")) {
                    MainData data=new MainData();
                    data.setText(sText);
                    database.mainDao().insert(data);
                    eT.setText("");
                    dataList.clear();
                    dataList.addAll(database.mainDao().getAll());
                    adapter.notifyDataSetChanged();
                }
            }
        });

        btnR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                database.mainDao().reset(dataList);
                dataList.clear();
                dataList.addAll(database.mainDao().getAll());
                adapter.notifyDataSetChanged();
            }
        });



    }

    public void openHome(View view){
        Intent intent=new Intent(this,HomeActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
