package lk.sylanka.shylankatoursapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.animation.ArgbEvaluator;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

public class Trip extends AppCompatActivity {

    ViewPager viewPager;
    Adapter adapter;
    Integer[] colors=null;
    ArgbEvaluator argbEvaluator=new ArgbEvaluator();
    List<Model> models;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip);

        findViewById(R.id.btnOrder).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Trip.this,Info.class);
                startActivity(intent);
            }
        });

        models=new ArrayList<>();
        models.add(new Model(R.drawable.h,"Hill Country","The train ride from Nanu Oya to Ella through forests and hills is acknowledged as one of the World’s great train trips. There are views too to be seen from the hill country village of Ella with its quaint cafés and scenic waterfalls. See the beauty of Sri Lanka’s hill country."));
        models.add(new Model(R.drawable.c,"Commercial Colombo","The central nerve of Sri Lanka’s commercial district, Colombo is located close to the airport. The city center is an eclectic blend of contemporary design sitting side by side with ancient temples and monuments Experience the pulsating nightlife or dine."));
        models.add(new Model(R.drawable.b,"Coastal Sri Lanka","We know the best beaches, hidden bays and secret coves for sunbathing, swimming or surfing to reward you with an unforgettable beach holiday with wide stretches of sand or palm-fringed secluded bays, Sri Lanka’s beaches are lapped by shallow lagoons or high, curling waves"));
        models.add(new Model(R.drawable.w,"Wild Side Of Ceylon","Sri Lanka is home to many animals including elephants, turtles and blue whales which are the main attractions, This is a must for all wildlife enthusiasts. The many national parks are home to majestic creatures, Our tours include many possibilities for wildlife watching and birding."));

        adapter=new Adapter(models,this);
        viewPager=findViewById(R.id.viewPager);
        viewPager.setAdapter(adapter);
        viewPager.setPadding(130,0,130,0);

        Integer[] colors_temp={
                getResources().getColor(R.color.color4),
                getResources().getColor(R.color.color5),
                getResources().getColor(R.color.color6),
                getResources().getColor(R.color.color3)
        };

        colors = colors_temp;

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if(position<(adapter.getCount()-1) && position<(colors.length-1)){
                    viewPager.setBackgroundColor((Integer) argbEvaluator.evaluate(positionOffset,colors[position],colors[position+1]));
                }
                else
                {
                    viewPager.setBackgroundColor(colors[colors.length-1]);
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
}

